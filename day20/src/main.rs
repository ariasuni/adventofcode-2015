use std::io::{self, Read};

fn main() {
    let mut input = String::new();
    let _ = io::stdin().read_to_string(&mut input);
    let goal = input.parse::<usize>().unwrap();
    // A number can be always be divided by itself and 1 (we ignore 1)
    // So, a house x has always at least (x + 1) * 10 presents
    // We can conclude that the answer can be at most (if the input is prime):
    let max = (goal / 10) - 1;

    println!("Part One: {}", part_one(goal, max));
    println!("Part Two: {}", part_two(goal, max));
}

fn part_one(goal: usize, max: usize) -> usize {
    let mut houses = Vec::new();
    houses.resize(max, 1);

    for elf_num in 2..max {
        for house_num in (elf_num..max).step_by(elf_num) {
            houses[house_num] += elf_num * 10;
        }
    }
    for (house_num, house) in houses.iter().enumerate().take(max).skip(2) {
        if *house >= goal {
            return house_num;
        }
    }
    unreachable!()
}

fn part_two(goal: usize, max: usize) -> usize {
    let mut houses = Vec::new();
    houses.resize(max, 1);

    for elf_num in 2..max {
        for house_num in (elf_num..max).step_by(elf_num).take(50) {
            houses[house_num] += elf_num * 11;
        }
    }
    for (house_num, house) in houses.iter().enumerate().take(max).skip(2) {
        if *house >= goal {
            return house_num;
        }
    }
    unreachable!()
}
