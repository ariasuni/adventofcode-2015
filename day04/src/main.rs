use crypto::digest::Digest;
use crypto::md5::Md5;
use std::io::{self, Read};

fn main() {
    let mut input = String::new();
    let _ = io::stdin().read_to_string(&mut input);
    let input = input.trim();
    let first_result = find_zeros(input, 0, 5);
    println!("Part One: {}", first_result);
    println!("Part Two: {}", find_zeros(input, first_result, 6));
}

fn find_zeros(input: &str, last: u32, times: u32) -> u32 {
    let mut sh = Md5::new();
    let pattern: String = "0".repeat(times as usize);
    let mut i = last;
    loop {
        sh.input_str(input);
        sh.input_str(&i.to_string());
        let output = sh.result_str();
        if output[0..times as usize] == pattern {
            return i;
        }
        sh.reset();
        i += 1;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn part_one() {
        let tests = [("abcdef", 609043), ("pqrstuv", 1048970)];
        for (input, expected) in &tests {
            let result = find_zeros(input, 0, 5);
            assert_eq!(result, *expected, "for input `{:#?}`", input);
        }
    }
}
