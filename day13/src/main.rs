use std::cmp::{max, min};
use std::collections::{HashMap, HashSet};
use std::io::{self, BufRead, BufReader};

use permutohedron::Heap;

fn main() {
    let reader = BufReader::new(io::stdin());
    let mut names = HashSet::new();
    let mut data = HashMap::new();

    for line in reader.lines() {
        let l = line.unwrap();
        let mut split = l.split(' ');

        let person1 = split.next().unwrap().to_string();
        let gain = if split.nth(1).unwrap() == "gain" {
            1
        } else {
            -1
        };
        let value = split.next().unwrap().parse::<i32>().unwrap() * gain;
        let mut person2 = split.nth(6).unwrap().to_string();
        person2.pop(); // remove the point at the end of the sentence

        names.insert(person1.clone());
        names.insert(person2.clone());
        let person2_hm = data.entry(person1).or_insert_with(HashMap::new);
        person2_hm.insert(person2, value);
    }

    let mut names: Vec<_> = names.iter().collect();
    let heap = Heap::new(&mut names);
    let mut max_hpns = 0; // hpns = happiness
    let mut max_hpns_with_self = 0;

    for config in heap {
        let mut hpns =
            data[config[0]][config[config.len() - 1]] + data[config[config.len() - 1]][config[0]];
        let mut min_hpns = hpns;
        for couple in config.windows(2) {
            let acc_hpns = data[couple[0]][couple[1]] + data[couple[1]][couple[0]];
            min_hpns = min(acc_hpns, min_hpns);
            hpns += acc_hpns;
        }
        max_hpns = max(hpns, max_hpns);
        max_hpns_with_self = max(hpns - min_hpns, max_hpns_with_self)
    }

    println!("Part One: {}", max_hpns);
    println!("Part Two: {}", max_hpns_with_self);
}
