use std::cmp::max;
use std::io::{self, BufRead, BufReader};
use std::str::FromStr;

fn main() {
    let reader = BufReader::new(io::stdin());

    let mut ingredients = vec![];
    for line in reader.lines() {
        let l = line.unwrap();
        let props: Vec<_> = l
            .split(':')
            .nth(1) // take the string after the ingredient’s name
            .unwrap()
            .split(',') // splits ingredient’s properties
            .map(|s| i32::from_str(s.split(' ').rev().next().unwrap()).unwrap())
            .collect();
        ingredients.push(props);
    }

    let mut score_max = 0;
    let mut score_max_with_500_calories = 0;
    let mut recipe = vec![0; ingredients.len()];
    *recipe.last_mut().unwrap() = 100;
    while let Some(recipe) = next_composition(&mut recipe) {
        let recipe_props = (0usize..5)
            .map(|prop_idx| {
                ingredients
                    .iter()
                    .enumerate()
                    // ignore ingredients not added
                    .filter(|(num, _)| recipe[*num] != 0)
                    .map(|(num, ing)| ing[prop_idx] * recipe[num] as i32)
                    .sum::<i32>()
            })
            .map(|sum| max(0, sum)) // properties must at least be zero
            .collect::<Vec<_>>();
        let score = recipe_props[0..4].iter().product();
        score_max = max(score, score_max);
        if recipe_props[4] == 500 {
            score_max_with_500_calories = max(score, score_max_with_500_calories);
        }
    }

    println!("Part One: {}", score_max);
    println!("Part Two: {}", score_max_with_500_calories);
}

fn next_composition(arr: &mut [u32]) -> Option<&mut [u32]> {
    if arr.len() <= 1 {
        return None;
    }
    if arr[arr.len() - 1] == 0 {
        let mut i = arr.len() - 1;
        while arr[i] == 0 {
            if i == 1 {
                return None;
            }
            i -= 1;
        }
        arr[arr.len() - 1] += arr[i] - 1;
        arr[i] = 0;
        arr[i - 1] += 1;
    } else {
        arr[arr.len() - 1] -= 1;
        arr[arr.len() - 2] += 1;
    }
    Some(arr)
}
