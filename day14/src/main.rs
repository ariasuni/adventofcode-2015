use std::io::{self, BufRead, BufReader};

struct Reindeer {
    cycle: Box<dyn Iterator<Item = u32>>,
    distance: u32,
    score: u32,
}

const SECONDS: u32 = 2503;

fn main() {
    let reader = BufReader::new(io::stdin());
    let mut reindeers = Vec::new();

    for line in reader.lines() {
        let l = line.unwrap();
        let split: Vec<_> = l.split(' ').collect();

        let speed = split[3].parse().unwrap();
        let stamina = split[6].parse().unwrap();
        let rest_time: usize = split[13].parse().unwrap();

        let mut cycle = vec![speed; stamina];
        cycle.resize(stamina + rest_time, 0);

        reindeers.push(Reindeer {
            cycle: Box::new(cycle.into_iter().cycle()),
            distance: 0,
            score: 0,
        });
    }

    for _ in 0..SECONDS {
        let mut max_dist_i = 0;
        let mut max_dist = 0;
        for (i, reindeer) in reindeers.iter_mut().enumerate() {
            reindeer.distance += reindeer.cycle.next().unwrap();
            if reindeer.distance > max_dist {
                max_dist_i = i;
                max_dist = reindeer.distance;
            }
        }
        reindeers[max_dist_i].score += 1;
    }
    let max_distance = reindeers.iter().map(|r| r.distance).max().unwrap();
    let max_score = reindeers.iter().map(|r| r.score).max().unwrap();

    println!("Part One: {}", max_distance);
    println!("Part Two: {}", max_score);
}
