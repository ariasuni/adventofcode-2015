use std::io::{self, Read};

fn main() {
    let mut input = String::new();
    let _ = io::stdin().read_to_string(&mut input);

    input = gen_next_password(input.trim_end());
    println!("Part One: {}", input);

    input = gen_next_password(&input);
    println!("Part Two: {}", input);
}

fn gen_next_password(password: &str) -> String {
    let mut pass: Vec<char> = password.chars().collect();
    loop {
        increment(&mut pass);
        if has_suit(&pass) && has_pairs(&pass) {
            return pass.into_iter().collect();
        }
    }
}

fn increment(pass: &mut [char]) {
    let mut index = pass.len() - 1;
    loop {
        let mut inc = 1;
        match pass[index] {
            // to skip i, o and l
            'h' | 'n' | 'k' => {
                inc = 2;
            }
            'z' => {
                pass[index] = 'a';
                index -= 1;
                continue;
            }
            _ => {}
        }
        pass[index] = (pass[index] as u8 + inc) as char;
        break;
    }
}

fn has_suit(pass: &[char]) -> bool {
    for chars in pass.windows(3) {
        if (chars[0] as u8 == chars[1] as u8 - 1) && (chars[1] as u8 == chars[2] as u8 - 1) {
            return true;
        }
    }
    false
}

fn has_pairs(pass: &[char]) -> bool {
    let mut index = 0;
    let mut prev_pair = None;
    let mut pairs = 0;

    loop {
        if pass[index] == pass[index + 1] && Some(pass[index]) != prev_pair {
            pairs += 1;
            prev_pair = Some(pass[index]);
            index += 1;
        }
        index += 1;

        if pairs == 2 {
            return true;
        } else if index >= pass.len() - 1 {
            return false;
        }
    }
}
