use std::collections::HashMap;
use std::io::{self, Read};

fn main() {
    let mut input = String::new();
    let _ = io::stdin().read_to_string(&mut input);
    let (year1, year2) = present_count(&input);
    println!("Part One: {}", year1);
    println!("Part Two: {}", year2);
}

fn present_count(input: &str) -> (usize, usize) {
    let mut coord_year1 = [0, 0];
    let mut houses_year1 = HashMap::new();

    let mut santa_num = 0; // 0 is Santa, 1 is Robo-Santa
    let mut coord_year2 = [[0, 0], [0, 0]];
    let mut houses_year2 = HashMap::new();
    houses_year1.insert([0, 0], 1);
    houses_year2.insert([0, 0], 1);

    for c in input.chars() {
        let (index, value) = match c {
            '^' => (0, 1),
            'v' => (0, -1),
            '>' => (1, 1),
            '<' => (1, -1),
            _ => break,
        };
        coord_year1[index] += value;
        coord_year2[santa_num][index] += value;
        *houses_year1.entry(coord_year1).or_insert(0) += 1;
        *houses_year2.entry(coord_year2[santa_num]).or_insert(0) += 1;
        santa_num = (santa_num + 1) % 2;
    }
    (
        houses_year1.values().filter(|x| **x >= 1).count(),
        houses_year2.values().filter(|x| **x >= 1).count(),
    )
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn part_one() {
        let tests = [(2, ">"), (4, "^>v<"), (2, "^v^v^v^v^v")];
        for (expected, input) in &tests {
            let (year1, _) = present_count(&input);
            assert_eq!(*expected, year1, "for input {}", input);
        }
    }

    #[test]
    fn part_two() {
        let tests = [(3, "^v"), (3, "^>v<"), (11, "^v^v^v^v^v")];
        for (expected, input) in &tests {
            let (_, year2) = present_count(&input);
            assert_eq!(*expected, year2, "for input {}", input);
        }
    }
}
