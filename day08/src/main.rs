use std::io::{self, Read};

fn main() {
    let mut input = String::new();
    let _ = io::stdin().read_to_string(&mut input);
    let lines = input.lines().collect::<Vec<_>>();

    let mut result: u32 = lines.iter().map(|x| diff_real_mem(x)).sum();
    println!("Part One: {}", result);
    result = lines.iter().map(|x| diff_real_escape(x)).sum();
    println!("Part Two: {}", result);
}

fn diff_real_mem(string: &str) -> u32 {
    let v = string.chars().collect::<Vec<_>>();
    let mut result = v.len() as u32;
    let mut i = 0;
    while i < v.len() {
        result -= match v[i] {
            '\\' => {
                i += match v[i + 1] {
                    'x' => 3,
                    _ => 1,
                };
                1
            }
            '"' => 0,
            _ => 1,
        };
        i += 1;
    }
    result
}

fn diff_real_escape(string: &str) -> u32 {
    string.chars().fold(2, |acc, c| match c {
        '"' | '\\' => acc + 1,
        _ => acc,
    })
}
