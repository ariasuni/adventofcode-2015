use std::io::{self, Read};

fn main() {
    let mut input = String::new();
    let _ = io::stdin().read_to_string(&mut input);
    let (last_floor, enter_basement_idx) = part_one_and_two(&input);
    println!("Part One: {}", last_floor);
    println!("Part Two: {}", enter_basement_idx);
}

fn part_one_and_two(input: &str) -> (i32, usize) {
    let mut floor = 0;
    let mut enter_basement_idx = 0;
    for (i, c) in input.chars().enumerate() {
        match c {
            '(' => floor += 1,
            ')' => floor -= 1,
            '\n' => {}
            _ => panic!(),
        }
        if enter_basement_idx == 0 && floor == -1 {
            enter_basement_idx = i + 1;
        }
    }
    (floor, enter_basement_idx)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn part_one() {
        let tests = [
            ("(())", 0),
            ("()()", 0),
            ("(((", 3),
            ("))(((((", 3),
            ("())", -1),
            ("))(", -1),
            (")))", -3),
            (")())())", -3),
        ];
        for (input, expected) in &tests {
            let (floor, _) = part_one_and_two(input);
            assert_eq!(floor, *expected, "for input `{:#?}`", input);
        }
    }

    #[test]
    fn part_two() {
        let tests = [(")", 1), ("()())", 5)];
        for (input, expected) in &tests {
            let (_, enter_basement_idx) = part_one_and_two(input);
            assert_eq!(enter_basement_idx, *expected, "for input `{:#?}`", input);
        }
    }
}
