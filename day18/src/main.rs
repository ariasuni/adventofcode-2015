use std::io::{self, BufRead, BufReader};

fn main() {
    let reader = BufReader::new(io::stdin());
    let grid: Vec<_> = reader
        .lines()
        .map(|l| l.unwrap().chars().map(|c| c == '#').collect::<Vec<_>>())
        .collect();

    println!("Part One: {}", game_of_life_count(&grid, false));
    println!("Part Two: {}", game_of_life_count(&grid, true));
}

#[allow(clippy::ptr_arg)]
fn game_of_life_count(grid: &Vec<Vec<bool>>, corner_stuck: bool) -> i32 {
    let mut grid = grid.clone();
    for _ in 0..100 {
        let old_grid = grid.clone();
        for y in 0..grid.len() {
            for x in 0..grid[0].len() {
                let count = neighbors_count(&old_grid, x, y, corner_stuck);
                grid[x][y] = count == 3 || (old_grid[x][y] && count == 2);
            }
        }
    }

    grid.iter()
        .flat_map(|l| l.iter())
        .fold(0, |acc, x| if *x { acc + 1 } else { acc })
}

#[allow(clippy::ptr_arg)]
fn neighbors_count(grid: &Vec<Vec<bool>>, x: usize, y: usize, corner_stuck: bool) -> u32 {
    let has_left = x > 0;
    let has_right = x < grid.len() - 1;
    let has_up = y > 0;
    let has_down = y < grid[0].len() - 1;

    if corner_stuck && (!has_left || !has_right) && (!has_up || !has_down) {
        return 3; // 3 neighbors always result to the light being on
    }

    let mut neighbors = 0;

    if has_left && grid[x - 1][y] {
        neighbors += 1;
    }
    if has_up && grid[x][y - 1] {
        neighbors += 1;
    }
    if has_right && grid[x + 1][y] {
        neighbors += 1;
    }
    if has_down && grid[x][y + 1] {
        neighbors += 1;
    }

    if has_left && has_up && grid[x - 1][y - 1] {
        neighbors += 1;
    }
    if has_right && has_up && grid[x + 1][y - 1] {
        neighbors += 1;
    }
    if has_right && has_down && grid[x + 1][y + 1] {
        neighbors += 1;
    }
    if has_left && has_down && grid[x - 1][y + 1] {
        neighbors += 1;
    }

    neighbors
}
