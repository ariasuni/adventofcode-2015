use lazy_static::lazy_static;
use regex::Regex;
use std::io::{self, BufRead, BufReader};

fn main() {
    let reader = BufReader::new(io::stdin());
    let mut count = 0;
    let mut count2 = 0;

    for line in reader.lines() {
        let l = line.unwrap();
        if is_nice_part_one(&l) {
            count += 1;
        }
        if is_nice_part_two(&l) {
            count2 += 1;
        }
    }

    println!("Part One: {}", count);
    println!("Part Two: {}", count2);
}

fn is_nice_part_one(s: &str) -> bool {
    has_three_vowels(s) && !is_bad(s) && has_pair(s)
}

fn is_nice_part_two(s: &str) -> bool {
    has_two_pairs(s) && has_twice_separated(s)
}

fn has_three_vowels(s: &str) -> bool {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"[aeiou].*[aeiou].*[aeiou]").unwrap();
    }
    RE.is_match(s)
}

fn is_bad(s: &str) -> bool {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"ab|cd|pq|xy").unwrap();
    }
    RE.is_match(s)
}

fn has_pair(s: &str) -> bool {
    let mut prev_c = '\0';
    for c in s.chars() {
        if c == prev_c {
            return true;
        }
        prev_c = c;
    }
    false
}

fn has_two_pairs(s: &str) -> bool {
    let mut prev_c = '\0';
    let mut prev2_c = prev_c;
    for c in s.chars() {
        if prev2_c == c {
            return true;
        }
        prev2_c = prev_c;
        prev_c = c;
    }
    false
}

fn has_twice_separated(s: &str) -> bool {
    let s: Vec<_> = s.chars().collect();
    for (i1, c1) in s.windows(2).enumerate() {
        for c2 in s[i1 + 2..s.len()].windows(2) {
            if c1[0] == c2[0] && c1[1] == c2[1] {
                return true;
            }
        }
    }
    false
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn part_one_ex1() {
        let input = "ugknbfddgicrmopn";
        assert_eq!(has_three_vowels(input), true, "for input `{:#?}`", input);
        assert_eq!(has_pair(input), true, "for input `{:#?}`", input);
        assert_eq!(is_bad(input), false, "for input `{:#?}`", input);
        assert_eq!(is_nice_part_one(input), true, "for input `{:#?}`", input);
    }

    #[test]
    fn part_one_ex2() {
        let input = "aaa";
        assert_eq!(has_three_vowels(input), true, "for input `{:#?}`", input);
        assert_eq!(has_pair(input), true, "for input `{:#?}`", input);
        assert_eq!(is_bad(input), false, "for input `{:#?}`", input);
        assert_eq!(is_nice_part_one(input), true, "for input `{:#?}`", input);
    }

    #[test]
    fn part_one_ex3() {
        let input = "jchzalrnumimnmhp";
        assert_eq!(has_three_vowels(input), true, "for input `{:#?}`", input);
        assert_eq!(has_pair(input), false, "for input `{:#?}`", input);
        assert_eq!(is_bad(input), false, "for input `{:#?}`", input);
        assert_eq!(is_nice_part_one(input), false, "for input `{:#?}`", input);
    }

    #[test]
    fn part_one_ex4() {
        let input = "haegwjzuvuyypxyu";
        assert_eq!(has_three_vowels(input), true, "for input `{:#?}`", input);
        assert_eq!(has_pair(input), true, "for input `{:#?}`", input);
        assert_eq!(is_bad(input), true, "for input `{:#?}`", input);
        assert_eq!(is_nice_part_one(input), false, "for input `{:#?}`", input);
    }

    #[test]
    fn part_one_ex5() {
        let input = "dvszwmarrgswjxmb";
        assert_eq!(has_three_vowels(input), false, "for input `{:#?}`", input);
        assert_eq!(has_pair(input), true, "for input `{:#?}`", input);
        assert_eq!(is_bad(input), false, "for input `{:#?}`", input);
        assert_eq!(is_nice_part_one(input), false, "for input `{:#?}`", input);
    }
}
