use std::io::{self, BufRead, BufReader};

const SIZE: usize = 1000;

fn main() {
    let reader = BufReader::new(io::stdin());
    let mut grid = vec![vec![false; SIZE]; SIZE];
    let mut grid2 = vec![vec![0i32; SIZE]; SIZE];
    for line in reader.lines() {
        let l = line.unwrap();
        let mut split = l.split_whitespace();
        let action = match split.next().unwrap() {
            "turn" => split.next().unwrap(),
            s => s,
        };
        let level = match action {
            "on" => 1,
            "off" => -1,
            "toggle" => 2,
            _ => panic!("Instruction invalide"),
        };
        let (x1, y1) = coord_from_str(split.next().unwrap());
        let (x2, y2) = coord_from_str(split.nth(1).unwrap()); // skip "through"
        for line in &mut grid[x1..=x2] {
            for light in &mut line[y1..=y2] {
                *light = match action {
                    "on" => true,
                    "off" => false,
                    "toggle" => !*light,
                    _ => panic!("Instruction invalide"),
                };
            }
        }
        for line in &mut grid2[x1..=x2] {
            for light in &mut line[y1..=y2] {
                *light += level;
                if *light < 0 {
                    *light = 0;
                }
            }
        }
    }
    println!(
        "Part One: {}",
        grid.iter()
            .flat_map(|line| line.iter())
            .fold(0, |sum, x| if x == &true { sum + 1 } else { sum })
    );
    println!(
        "Part Two: {}",
        grid2.iter().flat_map(|line| line.iter()).sum::<i32>()
    );
}

fn coord_from_str(s: &str) -> (usize, usize) {
    let v = s.split(',').map(|u| u.parse().unwrap()).collect::<Vec<_>>();
    (v[0], v[1])
}
