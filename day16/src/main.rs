use std::io::{self, BufRead, BufReader};
use std::str::FromStr;

use maplit::hashmap;

fn main() {
    let to_match_features = hashmap! {
        "children".to_string() => 3,
        "cats".to_string() => 7,
        "samoyeds".to_string() => 2,
        "pomeranians".to_string() => 3,
        "akitas".to_string() => 0,
        "vizslas".to_string() => 0,
        "goldfish".to_string() => 5,
        "trees".to_string() => 3,
        "cars".to_string() => 2,
        "perfumes".to_string() => 1,
    };
    let reader = BufReader::new(io::stdin());

    let mut sues = vec![];
    for line in reader.lines() {
        let l = line.unwrap();
        let mut it = l.split(':');
        it.next();

        let mut features = hashmap! {};
        let rest = it.collect::<String>();
        for feature_text in rest.split(',') {
            let mut parts = feature_text.trim().split(' ');
            let feature_name = parts.next().unwrap().to_string();
            if to_match_features.contains_key(&feature_name) {
                features.insert(feature_name, u32::from_str(parts.next().unwrap()).unwrap());
            }
        }
        sues.push(features);
    }

    for (index, features) in sues.iter().enumerate() {
        if features.iter().all(|(k, v)| to_match_features[k] == *v) {
            println!("Part One: {}", index + 1);
            break;
        }
    }

    for (index, features) in sues.iter().enumerate() {
        if features.iter().all(|(k, v)| match k.as_str() {
            "cats" | "trees" => *v > to_match_features[k],
            "pomeranians" | "goldfish" => *v < to_match_features[k],
            _ => to_match_features[k] == *v,
        }) {
            println!("Part Two: {}", index + 1);
            break;
        }
    }
}
