use std::io::{self, Read};

fn main() {
    let mut input = String::new();
    let _ = io::stdin().read_to_string(&mut input);
    let mut v: Vec<_> = input.trim_end().chars().collect();

    for time in 0..50 {
        let mut new_v = Vec::new();
        let mut count = 1;
        for c in v.windows(2) {
            if c[1] != c[0] {
                new_v.extend(count.to_string().chars());
                new_v.push(c[0]);
                count = 0;
            }
            count += 1;
        }
        new_v.extend(count.to_string().chars());
        new_v.push(*v.last().unwrap());
        v = new_v;
        if time == 39 {
            println!("Part One: {}", v.len());
        }
    }
    println!("Part Two: {}", v.len());
}
