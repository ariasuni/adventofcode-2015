# Advent of Code 2015 in Rust

This repository is organised as a workspace containing a Rust package per day. Each package’s directory contains my personal `input` of the day for practicality.

To run my solutions, [install Rust](https://www.rust-lang.org/install.html), then run e.g.: `cargo run --bin day01 --release < day01/input`. The executable then prints the two solutions in the terminal.
