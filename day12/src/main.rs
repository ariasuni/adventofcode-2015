use serde_json::{Map, Value};
use std::io::{self, Read};

fn main() {
    let mut input = String::new();
    let _ = io::stdin().read_to_string(&mut input);
    let json = serde_json::from_str(&input).unwrap();

    println!("Part One: {}", parse_json(0, &json, false));
    println!("Part Two: {}", parse_json(0, &json, true));
}

fn parse_json(acc: i64, json: &Value, ignore_red: bool) -> i64 {
    match json {
        Value::Number(num) => acc + num.as_i64().unwrap(),
        Value::Array(ref arr) => arr
            .iter()
            .fold(acc, |acc2, obj| parse_json(acc2, obj, ignore_red)),
        Value::Object(ref obj) if !(ignore_red && contain_red(obj)) => obj
            .values()
            .fold(acc, |acc2, obj| parse_json(acc2, obj, ignore_red)),
        _ => acc,
    }
}

fn contain_red(json_obj: &Map<String, Value>) -> bool {
    json_obj
        .values()
        .any(|v| *v == Value::String("red".to_string()))
}
