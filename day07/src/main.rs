use std::collections::HashMap;
use std::io::{self, BufRead, BufReader, Read};

fn main() {
    let mut parser = Parser::new();
    parser.feed(BufReader::new(io::stdin()));
    let (result1, result2) = parser.get_values("a");
    println!("Part One: {}", result1);
    println!("Part Two: {}", result2);
}

#[derive(Clone)]
enum Expr {
    InfixOp(String, String, String),
    Token(String),
}

struct Parser {
    wire_exprs: HashMap<String, Expr>,
    wire_values: HashMap<String, u32>,
}

impl Parser {
    fn new() -> Parser {
        Parser {
            wire_exprs: HashMap::new(),
            wire_values: HashMap::new(),
        }
    }

    fn feed<T: Read>(&mut self, reader: BufReader<T>) {
        for line in reader.lines() {
            let l = line.unwrap();
            let mut tokens: Vec<&str> = l.split(' ').collect();
            let wire = tokens.pop().unwrap().to_string();
            tokens.pop(); // get rid of "->"
            let expr = match tokens.len() {
                1 => Expr::Token(tokens[0].to_string()),
                2 => Expr::InfixOp(
                    tokens[1].to_string(),
                    "XOR".to_string(),
                    u32::max_value().to_string(),
                ),
                3 => Expr::InfixOp(
                    tokens[0].to_string(),
                    tokens[1].to_string(),
                    tokens[2].to_string(),
                ),
                _ => panic!(
                    "Unexpected number of tokens in expression `{}`",
                    tokens.concat()
                ),
            };
            self.wire_exprs.insert(wire, expr);
        }
    }

    fn get_values(&mut self, name: &str) -> (u32, u32) {
        let expr = self.wire_exprs[name].clone();
        let first_res = self.evaluate(&expr);
        self.wire_values.clear();
        self.wire_values.insert("b".to_string(), first_res);
        (first_res, self.evaluate(&expr))
    }

    fn evaluate(&mut self, expr: &Expr) -> u32 {
        match *expr {
            Expr::InfixOp(ref s1, ref op, ref s2) => Parser::compute(
                op,
                self.evaluate(&Expr::Token(s1.to_owned())),
                self.evaluate(&Expr::Token(s2.to_owned())),
            ),
            Expr::Token(ref s) => match s.parse::<u32>() {
                Ok(u) => u,
                _ => {
                    if !self.wire_values.contains_key(s) {
                        let expr = self.wire_exprs[s].clone();
                        let res = self.evaluate(&expr);
                        self.wire_values.insert(s.to_owned(), res);
                    }
                    self.wire_values[s]
                }
            },
        }
    }

    fn compute(op: &str, var1: u32, var2: u32) -> u32 {
        match op {
            "AND" => var1 & var2,
            "OR" => var1 | var2,
            "XOR" => var1 ^ var2,
            "LSHIFT" => var1 << var2,
            "RSHIFT" => var1 >> var2,
            _ => panic!("Unrecognized operator: `{}`.", op),
        }
    }
}
