use std::io::{self, BufRead, BufReader};

use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
struct Item {
    cost: u32,
    damage: u32,
    armor: u32,
}

#[derive(Serialize, Deserialize, Debug)]
struct Shop {
    weapons: Vec<Item>,
    armors: Vec<Item>,
    rings: Vec<Item>,
}

#[derive(Clone)]
struct Attacker {
    hps: u32,
    damage: u32,
    armor: u32,
}

fn main() -> io::Result<()> {
    let reader = BufReader::new(io::stdin());
    let mut lines = reader.lines();
    let boss = Attacker {
        hps: parse_line(&mut lines, "Hit Points"),
        damage: parse_line(&mut lines, "Damage"),
        armor: parse_line(&mut lines, "Armor"),
    };

    let shop: Shop = toml::from_str(include_str!("shop.toml"))?;

    let (part_one, part_two) = min_and_max_gold_spent_to_win(&shop, &boss);
    println!("Part One: {}", part_one);
    println!("Part Two: {}", part_two);
    Ok(())
}

fn parse_line(iter: &mut impl Iterator<Item = io::Result<String>>, field: &str) -> u32 {
    iter.next()
        .unwrap()
        .unwrap()
        .split(&format!("{}: ", field))
        .nth(1)
        .unwrap()
        .parse()
        .unwrap()
}

fn min_and_max_gold_spent_to_win(shop: &Shop, boss_original: &Attacker) -> (u32, u32) {
    let mut min_spent = std::u32::MAX;
    let mut max_spent = 0;
    let mut player;
    let mut boss = boss_original.clone();

    for weapon in shop.weapons.iter() {
        for armor in shop.armors.iter() {
            for (ring1_index, ring1) in shop.rings[0..shop.rings.len() - 1].iter().enumerate() {
                // We can’t have two times the same item, so the 2nd ring is always after the 1st,
                // except if ring1_index is 0, because we can have two times “No ring”
                let ring2_index_start = if ring1_index == 0 { 0 } else { ring1_index + 1 };
                for ring2 in shop.rings[ring2_index_start..].iter() {
                    player = Attacker {
                        hps: 100,
                        damage: weapon.damage + ring1.damage + ring2.damage,
                        armor: armor.armor + ring1.armor + ring2.armor,
                    };
                    boss.hps = boss_original.hps;
                    let cost = weapon.cost + armor.cost + ring1.cost + ring2.cost;
                    if player_wins(&mut player, &mut boss) {
                        min_spent = std::cmp::min(min_spent, cost);
                    } else {
                        max_spent = std::cmp::max(max_spent, cost);
                    }
                }
            }
        }
    }

    (min_spent, max_spent)
}

fn player_wins(player: &mut Attacker, boss: &mut Attacker) -> bool {
    let player_real_damage = if player.damage > boss.armor {
        player.damage - boss.armor
    } else {
        1
    };
    let boss_real_damage = if boss.damage > player.armor {
        boss.damage - player.armor
    } else {
        1
    };

    loop {
        if player_real_damage >= boss.hps {
            return true;
        }
        boss.hps -= player_real_damage;
        if boss_real_damage >= player.hps {
            return false;
        }
        player.hps -= boss_real_damage;
    }
}
