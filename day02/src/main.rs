use std::io::{self, BufRead, BufReader};

fn main() {
    let reader = BufReader::new(io::stdin());
    let mut part_one_result = 0;
    let mut part_two_result = 0;
    for line in reader.lines() {
        let l = line.unwrap();
        let d = parse_line(&l);
        part_one_result += wrapping_paper_surface(&d);
        part_two_result += ribbon_surface(&d);
    }
    println!("Part One: {}", part_one_result);
    println!("Part Two: {}", part_two_result);
}

fn parse_line(l: &str) -> [u32; 3] {
    let mut it = l.split('x').take(3).map(|x| x.parse().unwrap());
    let mut d: [u32; 3] = [it.next().unwrap(), it.next().unwrap(), it.next().unwrap()];
    d.sort_unstable();
    d
}

fn wrapping_paper_surface(d: &[u32; 3]) -> u32 {
    3 * d[0] * d[1] + 2 * d[1] * d[2] + 2 * d[0] * d[2]
}

fn ribbon_surface(d: &[u32; 3]) -> u32 {
    d[0] * d[1] * d[2] + d[0] * 2 + d[1] * 2
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn part_one() {
        let tests = [(58, parse_line("2x3x4")), (43, parse_line("1x1x10"))];
        for (expected, input) in &tests {
            assert_eq!(
                *expected,
                wrapping_paper_surface(input),
                "for input {:?}",
                input
            );
        }
    }

    #[test]
    fn part_two() {
        let tests = [(34, parse_line("2x3x4")), (14, parse_line("1x1x10"))];
        for (expected, input) in &tests {
            assert_eq!(*expected, ribbon_surface(input), "for input {:?}", input);
        }
    }
}
