use std::cmp::Ordering;
use std::io::{self, BufRead, BufReader};
use std::str::FromStr;

fn main() {
    let reader = BufReader::new(io::stdin());
    let containers: Vec<_> = reader
        .lines()
        .map(|l| u32::from_str(&l.unwrap()).unwrap())
        .collect();

    let mut container_count = 0; // containers that can hold 150L
    let mut min_container_number = std::u32::MAX; // minimum of containers to hold 150L
    let mut min_container_count = 0; // min_container_number containers that can hold 150L
    for conf in 0..=2u32.pow(containers.len() as u32) {
        let mut acc = 0;
        let mut container_number = 0;
        for (i, c) in containers.iter().enumerate() {
            if (conf >> i) & 1 == 1 {
                acc += c;
                container_number += 1;
            }
        }
        if acc == 150 {
            container_count += 1;
            match container_number.cmp(&min_container_number) {
                Ordering::Less => {
                    min_container_number = container_number;
                    min_container_count = 1;
                }
                Ordering::Equal => min_container_count += 1,
                Ordering::Greater => {}
            }
        }
    }

    println!("Part One: {}", container_count);
    println!("Part Two: {}", min_container_count);
}
